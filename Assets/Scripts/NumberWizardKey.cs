﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class NumberWizardKey : MonoBehaviour {

    int max, min, current, last, turn;
    bool guessed = false;
    bool started = false;

    [TextArea(5,10)]
    public string strIntro;
    [TextArea(5, 10)]
    public string strTurn;
    [TextArea(5, 10)]
    public string strWin;
    [TextArea(5, 10)]
    public string strHeader;

    public bool MouseGame = false;

    [SerializeField] Text HeaderComponent;
    [SerializeField] Text TextComponent;
    [SerializeField] Text NumberComponent;

    [SerializeField] Button ButtonInfoComponent;
    [SerializeField] Button ButtonHigherComponent;
    [SerializeField] Button ButtonGuessComponent;
    [SerializeField] Button ButtonLowerComponent;
    
    private void Start()
    {
        NewGame();
    }

    // Update is called once per frame
    void Update () {
        Turn();
    }

    void NewGame(bool game = false)
    {
        guessed = false;
        started = false;
        max = 100;
        min = 0;
        current = (max + min) / 2;
        turn = 1;
        TextComponent.text = strIntro;
        HeaderComponent.text = "";
        NumberComponent.text = "";

        if (MouseGame) ShowButtons(false);
    }

    // if in game, show 3 buttons. if on intro text, show only info button
    void ShowButtons(bool game)
    {
        ButtonHigherComponent.gameObject.SetActive(game);
        ButtonGuessComponent.gameObject.SetActive(game);
        ButtonLowerComponent.gameObject.SetActive(game);
        ButtonInfoComponent.gameObject.SetActive(!game);
    }

    // keyboard turn
    void Turn()
    {
        if (MouseGame) return;

        if (!started)
        {
            if (Input.GetKeyUp(KeyCode.Return))
            {
                ConfirmStartOrEndGame();
            }
            return;
        }

        if (guessed)
        {
            if (Input.GetKeyUp(KeyCode.Return))
            {
                SceneManager.LoadScene("Menu");
            }
            return;
        }

        if (Input.GetKeyUp(KeyCode.Return) || min == max)
        {
            Win();
            return;
        }
        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            GuessHigher();
        }
        if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            GuessLower();
        }

    }


    public void ConfirmStartOrEndGame()
    {
        if (!started) {
            HeaderComponent.text = strHeader + turn.ToString();
            NumberComponent.text = current.ToString();
            TextComponent.text = strTurn;
            started = true;

            if (MouseGame) ShowButtons(true);
            }
        if (guessed) {
            SceneManager.LoadScene("Menu");
        }
    }

    public void GuessHigher()
    {
        min = Mathf.Min(current + 1, max);
        NewTurn();
    }

    public void GuessLower()
    {
        max = Mathf.Max(current - 1, min);
        NewTurn();
    }

    void NewTurn()
    {
        last = current;
        current = (max + min) / 2;
        if (last != current)
        {
            turn++;
            HeaderComponent.text = strHeader + turn;
            NumberComponent.text = current.ToString() + " min: " + min + ", max:" + max;
        }
        else {
            Win();
        }
    }

    public void Win()
    {
        HeaderComponent.text = "WIN at turn " + turn.ToString();
        TextComponent.text = strWin;
        NumberComponent.text = current.ToString();
        guessed = true;

        if (MouseGame) ShowButtons(false);
    }


}
