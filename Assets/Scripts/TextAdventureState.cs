﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "State")]
public class TextAdventureState : ScriptableObject
{

    [TextArea(5,10)] public string StateText;
    public string ChapterText;
    public int ChapterLevel;

    public TextAdventureState[] NextStates;
    public char [] NextStateKeys;
}


