﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoad : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    void Update() {

    }

     public void LoadNumberWizardKey()
    {
        print("SceneManager.LoadScene(NumberWizardKey");
        SceneManager.LoadScene("NumberWizardKey");
    }

    public void LoadNumberWizardMouse()
    {
        print("SceneManager.LoadScene(NumberWizardMouse");
        SceneManager.LoadScene("NumberWizardMouse");
    }
}
