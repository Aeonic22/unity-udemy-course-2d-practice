﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class TextAdventureEngine : MonoBehaviour {


    public TextAdventureState initialState;
    TextAdventureState state;
    int currentChapterLevel = 0;

    [SerializeField] Text textMain;
    [SerializeField] Text textHeader;

    // Use this for initialization
    void Start () {
        currentChapterLevel = 0;
        state = initialState;
    }
	
	// Update is called once per frame
	void Update () {

        for (int i = 0; i < state.NextStateKeys.Length; i++) {
            KeyCode keyCode = (KeyCode) Enum.Parse(typeof(KeyCode), state.NextStateKeys[i].ToString());
            if (Input.GetKeyDown(keyCode)) {
                state = state.NextStates[i];
                break;
             }


            //var x = KeyCode.N.ToString();
           // var y = Enum.Parse(typeof(KeyCode), "N");
           // int a = 3;
        }

        textMain.text = state.StateText;
        if (state.ChapterLevel > currentChapterLevel) textHeader.text = state.ChapterText;
		
	}
}
